package com.example.cart_service.controllers;

import com.example.cart_service.models.Cart;
import com.example.cart_service.models.CartItem;
import com.example.cart_service.repositories.CartItemRepository;
import com.example.cart_service.repositories.CartRepository;
import com.example.cart_service.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("cart_service")
public class CartController {
    @Autowired
    CartRepository repository;
    @Autowired
    CartItemRepository cartItemRepository;
    @Autowired
    CartService cartService;

    @GetMapping("carts")
    public ResponseEntity<?> getAllCart(@RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);
        Page<Cart> cartPage = cartService.findAllCart(currentPage - 1, pageSize);
        return ResponseEntity.ok( cartPage);
    }

    @PostMapping("create")
    public ResponseEntity<?> saveCart(@RequestBody Cart cart,@RequestHeader("userId") Long userId) {
        try {
            if (cart != null) {
                if(userId == null){
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Header userId is null");
                }
                cart.setUserId(userId);
                Cart saveCart = repository.save(cart);
                return ResponseEntity.status(HttpStatus.OK).body(saveCart);
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Can't save null cart");
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server!");
        }
    }

    @GetMapping("carts-user/{id}")
    public ResponseEntity<?> getCartByUser(@PathVariable("id") Long id) {
        try {
            Cart cart = repository.findCartByUserId(id);
            System.out.println(cart);
            if (cart == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cart not found");
            }

            ResponseEntity<Map<String, Object>> userResponse = cartService.getUserById(cart.getUserId());
            if (userResponse.getStatusCode() != HttpStatus.OK) {
                return userResponse;
            }

            Map<String, Object> mapCart = cartService.convertObjectToMap(cart);
            System.out.println(userResponse.getBody());
            mapCart.remove("userId");
            mapCart.put("user", userResponse.getBody());

            return ResponseEntity.status(HttpStatus.OK).body(mapCart);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server");
        }
    }
    @DeleteMapping("/delete-item/{id}")
    public ResponseEntity<?> deleteCartItem(@PathVariable("id") Long id) {
        try {
            CartItem cartItem = cartItemRepository.findById(id).orElse(null);
            if (cartItem == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("CartItem not found.");
            }
            cartItemRepository.deleteCartItemById(id);
            return ResponseEntity.status(HttpStatus.OK).body("Delete item successfully!");
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server.");
        }
    }
    @PutMapping("{id}/add-item")
    public ResponseEntity<?>addCartItem(@PathVariable("id")Long id,@RequestBody CartItem cartItem){
        try{
            Cart cart = repository.findById(id).orElse(null);
            if(cart == null){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not found Cart");
            }
            cart.getListCartItem().add(cartItem);
            Cart saveCart = repository.save(cart);
            return ResponseEntity.status(HttpStatus.OK).body(saveCart);
        }catch (Exception e){
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server!");
        }
    }

}
