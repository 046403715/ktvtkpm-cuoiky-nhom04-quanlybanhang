package com.example.cart_service.repositories;

import com.example.cart_service.models.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository extends JpaRepository<Cart,Long> {
    public Cart findCartByUserId(long id);
}
