package com.example.cart_service.services;

import com.example.cart_service.models.Cart;
import com.example.cart_service.models.CartItem;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface CartService {
    Page<Cart> findAllCart(int pageNo, int pageSize);
    ResponseEntity<Map<String,Object>> getUserById(long id);
    Map<String,Object> convertObjectToMap(Cart cart);
}
