package com.example.api_gateway.util;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.text.ParseException;
import java.util.Date;

@Component
public class JwtUtil {
    private static Logger logger = LoggerFactory.getLogger(JwtUtil.class);
    private static final String USER = "tien";
    private static final String SECRET = "hey Mr Tien the secrect length must be at least 256 bits" +
            " please no reveal!";

    //--------------------getExpirationDateFromToken-------------------------
    private Date getExpirationDateFromToken(JWTClaimsSet claims) {
        return claims != null ? claims.getExpirationTime() : new Date();
    }

    //--------------------getClaimsFromToken-------------------------
    private JWTClaimsSet getClaimsFromToken(String token) {
        JWTClaimsSet claims = null;
        try {
            SignedJWT signedJWT = SignedJWT.parse(token);
            JWSVerifier verifier = new MACVerifier(SECRET.getBytes());
            if (signedJWT.verify(verifier)) {
                claims = signedJWT.getJWTClaimsSet();
            }
        } catch (ParseException | JOSEException e) {
            logger.error(e.getMessage());
        }
        return claims;
    }

    //--------------------isTokenExpired-------------------------
    private boolean isTokenExpired(JWTClaimsSet claims) {
        return getExpirationDateFromToken(claims).after(new Date());
    }

    public Mono<Void> validateToken(ServerHttpResponse response, ServerWebExchange exchange, GatewayFilterChain chain, String token) {
        try {
            // Parse token and get the claims
            SignedJWT signedJWT = SignedJWT.parse(token);
            JWTClaimsSet claims = getClaimsFromToken(token);

            // Verify the token signature
            JWSVerifier verifier = new MACVerifier(SECRET.getBytes());
            if (!signedJWT.verify(verifier)) {
                return sendErrorResponse(response, HttpStatus.UNAUTHORIZED, "Signature verification failed");
            }
            // Check if token has expired
            if (!isTokenExpired(claims)) {
                return sendErrorResponse(response, HttpStatus.UNAUTHORIZED, "Token has expired");
            }
            JSONObject jsonObject = (JSONObject) claims.getClaim(USER);
            Long userId = (Long) jsonObject.get("userId");
            exchange.getRequest().mutate().header("userId", String.valueOf(userId)).build();
            return chain.filter(exchange);
        } catch (ParseException | JOSEException e) {
            logger.error("Error validating token: {}", e.getMessage());
            return sendErrorResponse(response, HttpStatus.UNAUTHORIZED, "Error validating token");
        }
    }

    private Mono<Void> sendErrorResponse(ServerHttpResponse response, HttpStatus status, String errorMessage) {
        response.setStatusCode(status);
        response.getHeaders().add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        DataBuffer buffer = response.bufferFactory().wrap(errorMessage.getBytes());
        return response.writeWith(Mono.just(buffer));
    }

    public boolean hasRole(String token, String role) {
        JSONArray authorities = new JSONArray();
        JWTClaimsSet claims = getClaimsFromToken(token);
        if (claims != null) {
            JSONObject jsonObject = (JSONObject) claims.getClaim(USER);
            authorities = (JSONArray) jsonObject.get("authorities");
        }

        for (Object authority : authorities) {
            if (authority.equals(role)) {
                return true;
            }
        }

        return false;
    }
}
