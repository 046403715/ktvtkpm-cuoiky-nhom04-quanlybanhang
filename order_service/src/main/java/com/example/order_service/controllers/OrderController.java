package com.example.order_service.controllers;

import com.example.order_service.models.Order;
import com.example.order_service.repositories.OrderRepository;
import com.example.order_service.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("order_service")
public class OrderController {
    @Autowired
    OrderRepository repository;
    @Autowired
    OrderService orderService;

    @GetMapping("orders")
    public ResponseEntity<?> getAllOrder(@RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);
        Page<Order> orderPage = orderService.findAllOrder(currentPage - 1, pageSize);

        return ResponseEntity.ok(orderPage);
    }

    @PostMapping("create")
    public ResponseEntity<?> saveOrder(@RequestBody Order order,@RequestHeader("userId") Long userId) {
        try {
            if (order != null) {
                if(userId == null){
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Header userId is null!");
                }
                order.setUserId(userId);
                Order saveOrder = repository.save(order);
                return ResponseEntity.status(HttpStatus.OK).body(saveOrder);
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Can't save null order");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server!");
        }
    }
    @GetMapping("orders-user/{id}")
    public ResponseEntity<?> getOrderByUser(@PathVariable("id") Long id, @RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);
        Page<Order> orderPage = orderService.findOrderByUserId(id, currentPage - 1, pageSize);
        return ResponseEntity.ok(orderPage);
    }
    @GetMapping("order/{id}")
    public ResponseEntity<?> getOrderById(@PathVariable("id") Long id) {
        try {
            Order order = repository.findById(id).orElse(null);
            if (order == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Order not found");
            }
            ResponseEntity<Map<String, Object>> userResponse = orderService.getUserById(order.getUserId());
            if (userResponse.getStatusCode() != HttpStatus.OK) {
                return userResponse;
            }

            Map<String, Object> mapOrder = orderService.convertObjectToMap(order);
            mapOrder.remove("userId");
            mapOrder.put("user", userResponse.getBody());

            return ResponseEntity.status(HttpStatus.OK).body(mapOrder);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server");
        }
    }
}
