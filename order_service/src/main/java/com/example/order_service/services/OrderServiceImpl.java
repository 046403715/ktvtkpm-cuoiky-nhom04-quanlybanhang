package com.example.order_service.services;

import com.example.order_service.models.Order;
import com.example.order_service.repositories.OrderRepository;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService{
    @Autowired
    OrderRepository repository;
    @Value("${user.service.url}")
    private String userServiceApi;
    RestTemplate restTemplate = new RestTemplate();
    @Override
    public Page<Order> findAllOrder(int pageNo, int pageSize) {
        int startItem = pageNo*pageSize;
        List<Order> list;
        List<Order> orders = repository.findAll();
        if(orders.size()< startItem){
            list = Collections.emptyList();
        }else{
            int toIndex = Math.min(startItem+ pageSize,orders.size());
            list = orders.subList(startItem,toIndex);
        }
        return new PageImpl<>(list, PageRequest.of(pageNo,pageSize),orders.size());
    }

    @Override
    public Page<Order> findOrderByUserId(long id,int pageNo, int pageSize) {
        int startItem = pageNo*pageSize;
        List<Order> list;
        List<Order> orders = repository.findOrderByUserId(id);
        if(orders.size()< startItem){
            list = Collections.emptyList();
        }else{
            int toIndex = Math.min(startItem+ pageSize,orders.size());
            list = orders.subList(startItem,toIndex);
        }
        return new PageImpl<>(list, PageRequest.of(pageNo,pageSize),orders.size());
    }

    @Override
    @Retry(name = "retryApi")
    @CircuitBreaker(name = "circuitBreakerApi",fallbackMethod = "circuitBreakerFallback")
    public ResponseEntity<Map<String, Object>> getUserById(long id) {
        String apiUrl = userServiceApi+"/user/"+id;
        ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(apiUrl,
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        return  responseEntity;
    }
    public ResponseEntity<String> circuitBreakerFallback(long id, CallNotPermittedException e) {
        // Return a suitable response when the circuit breaker is open
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("CircuitBreaker 'circuitBreakerApi' is OPEN and does not permit further calls");
    }

    @Override
    public Map<String, Object> convertObjectToMap(Order order) {
        Map<String, Object> map = new HashMap<>();
        // Lấy danh sách các trường của đối tượng
        Field[] fields = order.getClass().getDeclaredFields();

        for (Field field : fields) {
            try {
                // Cho phép truy cập vào các trường private
                field.setAccessible(true);
                // Đưa tên trường và giá trị của trường vào map
                map.put(field.getName(), field.get(order));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return map;
    }
}
