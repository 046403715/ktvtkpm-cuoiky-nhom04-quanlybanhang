package com.example.order_service.services;

import com.example.order_service.models.Order;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface OrderService {
    Page<Order> findAllOrder(int pageNo, int pageSize);
    Page<Order> findOrderByUserId(long id,int pageNo, int pageSize);
    ResponseEntity<Map<String,Object>> getUserById(long id);
    Map<String,Object> convertObjectToMap(Order order);
}
