package com.example.supplier_service.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "suppliers")
public class Supplier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "supplier_id")
    private Long id;
    @Column(name = "company_name", length = 150)
    private String companyName;
    @Column(name = "phone_number", length = 15)
    private String phoneNumber;
    @Column(length = 150)
    private String email;
    @Column(length = 250)
    private String address;
    @Column(length = 550)
    private String description;
    @Column(name = "web_url", length = 250)
    private String webURL;
}
