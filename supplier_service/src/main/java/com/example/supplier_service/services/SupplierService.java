package com.example.supplier_service.services;

import com.example.supplier_service.models.Supplier;
import org.springframework.data.domain.Page;

public interface SupplierService {
    Page<Supplier> findAllSupplier(int pageNo, int pageSize);
    Supplier updateSupplier(Supplier currentSupplier, Supplier updateSupplier);
    boolean isValidPhoneNumber(String phoneNumber);
    boolean isValidEmail(String email);
}
