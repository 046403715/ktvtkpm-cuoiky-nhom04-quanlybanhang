package com.example.supplier_service.services;

import com.example.supplier_service.models.Supplier;
import com.example.supplier_service.repositories.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

@Service
public class SupplierServiceImpl implements SupplierService{
    @Autowired
    private SupplierRepository supplierRepository;

    @Override
    public Page<Supplier> findAllSupplier(int pageNo, int pageSize) {
        int startItem = pageNo * pageSize;
        List<Supplier> list;
        List<Supplier> suppliers = supplierRepository.findAll();

        if (suppliers.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, suppliers.size());
            list = suppliers.subList(startItem, toIndex);
        }

        return new PageImpl<>(list, PageRequest.of(pageNo, pageSize), suppliers.size());
    }

    @Override
    public Supplier updateSupplier(Supplier currentSupplier, Supplier updateSupplier) {
        if (updateSupplier.getCompanyName() != null) {
            currentSupplier.setCompanyName(updateSupplier.getCompanyName());
        }

        if (updateSupplier.getEmail() != null) {
            currentSupplier.setEmail(updateSupplier.getEmail());
        }

        if (updateSupplier.getPhoneNumber() != null) {
            currentSupplier.setPhoneNumber(updateSupplier.getPhoneNumber());
        }

        if (updateSupplier.getAddress() != null) {
            currentSupplier.setAddress(updateSupplier.getAddress());
        }

        if (updateSupplier.getDescription() != null) {
            currentSupplier.setDescription(updateSupplier.getDescription());
        }

        if (updateSupplier.getWebURL() != null) {
            currentSupplier.setWebURL(updateSupplier.getWebURL());
        }

        return supplierRepository.save(currentSupplier);
    }

    @Override
    public boolean isValidPhoneNumber(String phoneNumber) {
        String regex = "(((\\+|)84)|0)(3|5|7|8|9)[0-9]{8}\\b";
        return Pattern.compile(regex).matcher(phoneNumber).matches();
    }

    @Override
    public boolean isValidEmail(String email) {
        String regex = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        return Pattern.compile(regex).matcher(email).matches();
    }
}
