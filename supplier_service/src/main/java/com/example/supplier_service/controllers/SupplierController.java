package com.example.supplier_service.controllers;

import com.example.supplier_service.models.Supplier;
import com.example.supplier_service.repositories.SupplierRepository;
import com.example.supplier_service.services.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("supplier_service")
public class SupplierController {
    @Autowired
    private SupplierRepository supplierRepository;
    @Autowired
    private SupplierService supplierService;

    @GetMapping("/suppliers")
    public ResponseEntity<?> getAllSuppliers(@RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        Page<Supplier> supplierPage = supplierService.findAllSupplier(currentPage - 1, pageSize);
        return ResponseEntity.ok(supplierPage);
    }

    @GetMapping("/supplier/{id}")
    public ResponseEntity<?> getSupplierById(@PathVariable("id") Long id) {
        // Giả lập một tình huống khi gặp lỗi
//        if (true) {
//            throw new RuntimeException("Failed to fetch products");
//        }
        try {
            Supplier supplier = supplierRepository.findById(id).orElse(null);
            if (supplier == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Supplier not found.");
            }
            return ResponseEntity.status(HttpStatus.OK).body(supplier);
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server.");
        }
    }

    @PostMapping("/create")
    public ResponseEntity<?> createSupplier(@RequestBody Supplier supplier) {
        try {
            if(supplier != null){
                if(supplier.getCompanyName() == null || supplier.getCompanyName().trim().isEmpty()){
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Company name is required.");
                }
                if((supplier.getPhoneNumber() == null || supplier.getPhoneNumber().trim().isEmpty()) && (supplier.getEmail() == null || supplier.getEmail().trim().isEmpty())) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Phone number or email is required.");
                }
                if(supplier.getPhoneNumber() != null && !supplierService.isValidPhoneNumber(supplier.getPhoneNumber())){
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid phone number.");
                }
                if(supplier.getEmail() != null &&  !supplierService.isValidEmail(supplier.getEmail())){
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid email.");
                }
                Supplier savedSupplier = supplierRepository.save(supplier);
                return ResponseEntity.status(HttpStatus.OK).body(savedSupplier);
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Can't save a null supplier");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server.");
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateSupplier(@PathVariable("id") Long id, @RequestBody Supplier supplier) {
        try {
            Supplier foundSupplier = supplierRepository.findById(id).orElse(null);
            if (foundSupplier == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Supplier not found.");
            }
            if(supplier.getCompanyName() != null && supplier.getCompanyName().trim().isEmpty()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Company name is required.");
            }
            if(
                //Nếu update phoneNumber và email và cả 2 cùng là chuỗi rỗng
                ((supplier.getPhoneNumber() != null && supplier.getPhoneNumber().trim().isEmpty()) && (supplier.getEmail() != null && supplier.getEmail().trim().isEmpty())) ||
                //Nếu update phoneNumber là chuỗi rỗng mà không update email và email của supplier hiện tại cũng là null
                ((supplier.getPhoneNumber() != null && supplier.getPhoneNumber().trim().isEmpty()) && supplier.getEmail() == null && (foundSupplier.getEmail() == null || foundSupplier.getEmail().trim().isEmpty())) ||
                //Nếu update email là chuỗi rỗng mà không update phoneNumber và phoneNumber của supplier hiện tại cũng là null
                ((supplier.getEmail() != null && supplier.getEmail().trim().isEmpty()) && supplier.getPhoneNumber() == null && (foundSupplier.getPhoneNumber() == null || foundSupplier.getPhoneNumber().trim().isEmpty()))
            )
            {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Phone number or email is required.");
            }
            if(supplier.getPhoneNumber() != null && !supplierService.isValidPhoneNumber(supplier.getPhoneNumber())){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid phone number.");
            }
            if(supplier.getEmail() != null &&  !supplierService.isValidEmail(supplier.getEmail())){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid email.");
            }
            Supplier updatedSupplier = supplierService.updateSupplier(foundSupplier, supplier);
            return ResponseEntity.status(HttpStatus.OK).body(updatedSupplier);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server.");
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteSupplier(@PathVariable("id") Long id) {
        try {
            Supplier supplier = supplierRepository.findById(id).orElse(null);
            if (supplier == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Supplier not found.");
            }
            supplierRepository.delete(supplier);
            return ResponseEntity.status(HttpStatus.OK).body("Delete supplier successfully!");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server.");
        }
    }
}
