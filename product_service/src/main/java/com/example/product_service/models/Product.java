package com.example.product_service.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "products")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private long id;
    @Column(name = "product_name")
    private String name;
    private double price;
    @Column(name = "supplier_id")
    private long supplierId;
    @Column(name = "product_image")
    private String productImage;
    private String description;
    private int quantity;
    private String unit;
    private ProductStatus status = ProductStatus.ACTIVE;

    public Product(String name, double price, long supplierId, String productImage, String description, int quantity, String unit, ProductStatus status) {
        this.name = name;
        this.price = price;
        this.supplierId = supplierId;
        this.productImage = productImage;
        this.description = description;
        this.quantity = quantity;
        this.unit = unit;
        this.status = status;
    }
}
