package com.example.product_service.services;

import com.example.product_service.models.Product;
import com.example.product_service.repositories.ProductRepository;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.RequestNotPermitted;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Field;
import java.util.*;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    ProductRepository repository;
    @Value("${supplier.service.url}")
    private String supplierServiceApi;
    RestTemplate restTemplate = new RestTemplate();

    //
    @Override
    public Page<Product> findAllProduct(int pageNo, int pageSize) {
        int startItem = pageNo * pageSize;
        List<Product> list;
        List<Product> products = repository.findAll();
        if (products.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, products.size());
            list = products.subList(startItem, toIndex);
        }
        return new PageImpl<>(list, PageRequest.of(pageNo, pageSize), products.size());
    }

    @Override
    public Product updateProduct(Product currentProduct, Product updateProduct) {
        if (updateProduct.getName() != null) {
            currentProduct.setName(updateProduct.getName());
        }
        if (updateProduct.getProductImage() != null) {
            currentProduct.setProductImage(updateProduct.getProductImage());
        }
        if (updateProduct.getDescription() != null) {
            currentProduct.setDescription(updateProduct.getDescription());
        }
        if (updateProduct.getUnit() != null) {
            currentProduct.setUnit(updateProduct.getUnit());
        }
        currentProduct.setPrice(updateProduct.getPrice());
        currentProduct.setQuantity(updateProduct.getQuantity());
        currentProduct.setSupplierId(updateProduct.getSupplierId());
        return repository.save(currentProduct);
    }

    @Override
    @Retry(name = "retryApi")
    @CircuitBreaker(name = "circuitBreakerApi", fallbackMethod = "circuitBreakerFallback")
    public ResponseEntity<Map<String, Object>> getSupplierById(long id) {
        String apiUrl = supplierServiceApi+"/supplier/" + id;
        ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(apiUrl,
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        return responseEntity;
    }

    public ResponseEntity<String> circuitBreakerFallback(long id, CallNotPermittedException e) {
        // Return a suitable response when the circuit breaker is open
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("CircuitBreaker 'circuitBreakerApi' is OPEN and does not permit further calls");
    }
    @Override
    public Map<String, Object> convertObjectToMap(Product product) {
        Map<String, Object> map = new HashMap<>();
        // Lấy danh sách các trường của đối tượng
        Field[] fields = product.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                // Cho phép truy cập vào các trường private
                field.setAccessible(true);
                // Đưa tên trường và giá trị của trường vào map
                map.put(field.getName(), field.get(product));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    @Override
    public boolean isValidPrice(double price) {
        return price > 0;
    }

    @Override
    public boolean isValidQuantity(int quantity) {
        return quantity >= 0;
    }
}
