package com.example.product_service.services;

import com.example.product_service.models.Product;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface ProductService {
    Page<Product> findAllProduct(int pageNo, int pageSize);
    Product updateProduct(Product currentProduct, Product updateProduct);
    ResponseEntity<Map<String, Object>> getSupplierById(long id);
    Map<String,Object> convertObjectToMap(Product product);
    boolean isValidPrice(double price);
    boolean isValidQuantity(int quantity);
}
