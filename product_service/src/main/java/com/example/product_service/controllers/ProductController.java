package com.example.product_service.controllers;

import com.example.product_service.models.Product;
import com.example.product_service.models.ProductStatus;
import com.example.product_service.repositories.ProductRepository;
import com.example.product_service.services.ProductService;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("product_service")
@RateLimiter(name = "rateLimiterApi")
public class ProductController {
    @Autowired
    ProductRepository repository;
    @Autowired
    ProductService productService;
    @GetMapping("products")
    public ResponseEntity<?> getAllProduct(@RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);
        Page<Product> productPage = productService.findAllProduct(currentPage - 1, pageSize);
        return ResponseEntity.ok(productPage);
    }

    @GetMapping("product/{id}")
    public ResponseEntity<?> getProductById(@PathVariable("id") Long id) {
        try {
            Product product = repository.findById(id).orElse(null);
            if (product == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found");
            }
            ResponseEntity<Map<String, Object>> supplierResponse = productService.getSupplierById(product.getSupplierId());
            if (supplierResponse.getStatusCode() != HttpStatus.OK) {
                return supplierResponse;
            }

            Map<String, Object> mapProduct = productService.convertObjectToMap(product);
            Map<String, Object> supplier = supplierResponse.getBody();
            mapProduct.remove("supplierId");
            mapProduct.put("supplier", supplier);

            return ResponseEntity.status(HttpStatus.OK).body(mapProduct);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server");
        }
    }

    @PostMapping("create")
    public ResponseEntity<?> createProduct(@RequestBody Product product) {
        try {
            if (product != null) {
                if (product.getName() == null || product.getName().trim().isEmpty()) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid product name");
                }
                if (!productService.isValidPrice(product.getPrice())) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid product price");
                }
                if (!productService.isValidQuantity(product.getQuantity())) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid product quantity");
                }
                Product saveProduct = repository.save(product);
                return ResponseEntity.status(HttpStatus.OK).body(saveProduct);
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Can't save null product");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server!");
        }
    }

    @PutMapping("update/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable("id") Long id, @RequestBody Product product) {
        try {
            Product foundProduct = repository.findById(id).orElse(null);
            if (foundProduct == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found");
            }
            if (!productService.isValidPrice(product.getPrice())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid price product");
            }
            if (!productService.isValidQuantity(product.getQuantity())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid quantity product");
            }
            Product updateProduct = productService.updateProduct(foundProduct, product);
            return ResponseEntity.status(HttpStatus.OK).body(updateProduct);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server!");
        }

    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteSupplier(@PathVariable("id") Long id) {
        try {
            Product product = repository.findById(id).orElse(null);
            if (product == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found.");
            }
            repository.delete(product);
            return ResponseEntity.status(HttpStatus.OK).body("Delete product successfully!");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server.");
        }
    }

    @GetMapping("/hello")
    public String sayHello() {
        return "Hi";
    }
}
