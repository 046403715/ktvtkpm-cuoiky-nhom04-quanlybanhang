package com.example.user_service.models;

import com.example.user_service.enums.UserStatus;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;
    @Column(name = "full_name", length = 150)
    private String fullName;
    private Date dob;
    private Boolean gender;
    @Column(name = "phone_number", length = 15)
    private String phoneNumber;
    @Column(length = 150)
    private String email;
    @Column(length = 250)
    private String address;
    private String avatar;
    @Column(length = 550)
    private String description;
    @Enumerated(EnumType.ORDINAL)
    private UserStatus status = UserStatus.ACTIVE;
    @Column(name = "account_id", nullable = false)
    private Long accountId;

    public User(Long accountId) {
        this.accountId = accountId;
    }

}
