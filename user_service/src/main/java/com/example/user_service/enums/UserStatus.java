package com.example.user_service.enums;

public enum UserStatus {
    ACTIVE,
    IN_ACTIVE
}
