package com.example.user_service.controllers;

import com.example.user_service.models.User;
import com.example.user_service.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/user_service")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public ResponseEntity<?> getAllUsers(@RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        Page<User> userPage = userService.findAllUser(currentPage - 1, pageSize);
        return ResponseEntity.ok(userPage);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<?> getUserById(@PathVariable("id") Long id) {
        try {
            User user = userService.findUserById(id);
            if (user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found.");
            }
            return ResponseEntity.status(HttpStatus.OK).body(user);
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server.");
        }
    }

    @PostMapping("/create")
    public ResponseEntity<?> createUser(@RequestBody User user) {
        // Giả lập một tình huống khi gặp lỗi
//        if (true) {
//            throw new RuntimeException("Failed to fetch products");
//        }
        try {
            if(user != null){
                if(user.getPhoneNumber() != null && !userService.isValidPhoneNumber(user.getPhoneNumber())){
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid phone number.");
                }
                if(user.getEmail() != null &&  !userService.isValidEmail(user.getEmail())){
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid email.");
                }
                User savedUser = userService.createUser(user);
                return ResponseEntity.status(HttpStatus.OK).body(savedUser);
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Can't save a null user");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server.");
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateUser(@PathVariable("id") Long id, @RequestBody User user) {
        try {
            User foundUser = userService.findUserById(id);
            if (foundUser == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found.");
            } else {
                if(user.getPhoneNumber() != null && !userService.isValidPhoneNumber(user.getPhoneNumber())){
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid phone number.");
                }
                if(user.getEmail() != null &&  !userService.isValidEmail(user.getEmail())){
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid email.");
                }
                User updatedUser = userService.updateUser(foundUser, user);
                return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server.");
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) {
        try {
            User user = userService.findUserById(id);
            if (user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found.");
            } else {
                userService.deleteUser(user);
                return ResponseEntity.status(HttpStatus.OK).body("Delete user successfully!");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server.");
        }
    }
}
