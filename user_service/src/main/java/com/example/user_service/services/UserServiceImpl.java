package com.example.user_service.services;

import com.example.user_service.enums.UserStatus;
import com.example.user_service.models.User;
import com.example.user_service.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;

//    @Value("${redis.host}")
//    private String redisHost;

    private Jedis jedis = new Jedis("redis", 6379);

    @Override
    public User findUserById(Long id) throws ParseException {
        String key = String.valueOf(id);

        if(jedis.exists(key)){
            User userInCache = new User();
            userInCache.setId(id);

            String fullNameStr = jedis.hget(key, "fullName");
            if (fullNameStr != null) {
                userInCache.setFullName(fullNameStr);
            }

            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dobStr = jedis.hget(key, "dob");
            if (dobStr != null) {
                Date dob = formatter.parse(dobStr);
                userInCache.setDob(dob);
            }

            String genderStr = jedis.hget(key, "gender");
            if (genderStr != null) {
                Boolean gender = Boolean.parseBoolean(genderStr);
                userInCache.setGender(gender);
            }

            String phoneNumberStr = jedis.hget(key, "phoneNumber");
            if (phoneNumberStr != null) {
                userInCache.setPhoneNumber(phoneNumberStr);
            }

            String emailStr = jedis.hget(key, "email");
            if (emailStr != null) {
                userInCache.setEmail(emailStr);
            }

            String addressStr = jedis.hget(key, "address");
            if (addressStr != null) {
                userInCache.setAddress(addressStr);
            }

            String avatarStr = jedis.hget(key, "avatar");
            if (avatarStr != null) {
                userInCache.setAvatar(avatarStr);
            }

            String descriptionStr = jedis.hget(key, "description");
            if (descriptionStr != null) {
                userInCache.setDescription(descriptionStr);
            }

            String statusStr = jedis.hget(key, "status");
            if (statusStr != null) {
                userInCache.setStatus(Enum.valueOf(UserStatus.class, statusStr));
            }

            String accountIdStr = jedis.hget(key, "accountId");
            if (statusStr != null) {
                userInCache.setAccountId(Long.parseLong(accountIdStr));
            }

            return userInCache;
        } else {
            User user = userRepository.findById(id).orElse(null);
            if (user != null){
                setUserIntoCache(key, user);
            }
            return user;
        }
    }

    @Override
    public User createUser(User user) {
        User savedUser = userRepository.save(user);
        String key = String.valueOf(savedUser.getId());
        setUserIntoCache(key, savedUser);

        return savedUser;
    }

    @Override
    public void deleteUser(User user) {
        userRepository.delete(user);
        if(jedis.exists(String.valueOf(user.getId()))){
            jedis.del(String.valueOf(user.getId()));
        }
    }

    @Override
    public User updateUser(User currentUser, User updateUser) {
        if (updateUser.getFullName() != null) {
            currentUser.setFullName(updateUser.getFullName());
        }

        if (updateUser.getGender() != null) {
            currentUser.setGender(updateUser.getGender());
        }

        if (updateUser.getDob() != null) {
            currentUser.setDob(updateUser.getDob());
        }

        if (updateUser.getPhoneNumber() != null) {
            currentUser.setPhoneNumber(updateUser.getPhoneNumber());
        }

        if (updateUser.getEmail() != null) {
            currentUser.setEmail(updateUser.getEmail());
        }

        if (updateUser.getAddress() != null) {
            currentUser.setAddress(updateUser.getAddress());
        }

        if (updateUser.getAvatar() != null) {
            currentUser.setAvatar(updateUser.getAvatar());
        }

        if (updateUser.getDescription() != null) {
            currentUser.setDescription(updateUser.getDescription());
        }

        if (updateUser.getStatus() != null) {
            currentUser.setStatus(updateUser.getStatus());
        }

        if(jedis.exists(String.valueOf(currentUser.getId()))){
            setUserIntoCache(String.valueOf(currentUser.getId()), currentUser);
        }

        return userRepository.save(currentUser);
    }

    @Override
    public Page<User> findAllUser(int pageNo, int pageSize) {
        int startItem = pageNo * pageSize;
        List<User> list;
        List<User> users = userRepository.findAll();

        if (users.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, users.size());
            list = users.subList(startItem, toIndex);
        }

        return new PageImpl<>(list, PageRequest.of(pageNo, pageSize), users.size());
    }

    @Override
    public boolean isValidPhoneNumber(String phoneNumber) {
        String regex = "(((\\+|)84)|0)(3|5|7|8|9)[0-9]{8}\\b";
        return Pattern.compile(regex).matcher(phoneNumber).matches();
    }

    @Override
    public boolean isValidEmail(String email) {
        String regex = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        return Pattern.compile(regex).matcher(email).matches();
    }

    private void setUserIntoCache(String key, User user) {
        if(user.getFullName() != null){
            jedis.hset(key, "fullName", user.getFullName());
        }
        if(user.getDob() != null){
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dobStr = formatter.format(user.getDob());
            jedis.hset(key, "dob", dobStr);
        }
        if(user.getGender() != null){
            jedis.hset(key, "gender", user.getGender().toString());
        }
        if(user.getPhoneNumber() != null){
            jedis.hset(key, "phoneNumber", user.getPhoneNumber());
        }
        if(user.getEmail() != null){
            jedis.hset(key, "email", user.getEmail());
        }
        if(user.getAddress() != null){
            jedis.hset(key, "address", user.getAddress());
        }
        if(user.getAvatar() != null){
            jedis.hset(key, "avatar", user.getAvatar());
        }
        if(user.getDescription() != null){
            jedis.hset(key, "description", user.getDescription());
        }
        jedis.hset(key, "status", user.getStatus().toString());
        if(user.getAccountId() != null){
            jedis.hset(key, "accountId", user.getAccountId().toString());
        }
    }
}
