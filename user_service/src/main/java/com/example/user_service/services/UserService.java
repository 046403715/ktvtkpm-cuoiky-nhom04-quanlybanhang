package com.example.user_service.services;

import com.example.user_service.models.User;
import org.springframework.data.domain.Page;

import java.text.ParseException;

public interface UserService {
    User findUserById(Long id) throws ParseException;
    User createUser(User user);
    void deleteUser(User user);
    User updateUser(User currentUser, User updateUser);
    Page<User> findAllUser(int pageNo, int pageSize);
    boolean isValidPhoneNumber(String phoneNumber);
    boolean isValidEmail(String email);
}
