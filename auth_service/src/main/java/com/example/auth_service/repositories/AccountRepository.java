package com.example.auth_service.repositories;

import com.example.auth_service.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository
        extends JpaRepository<Account, Long> {

    Account findByUsername(String username);
}
