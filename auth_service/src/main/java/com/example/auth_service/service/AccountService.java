package com.example.auth_service.service;

import com.example.auth_service.authen.UserPrincipal;
import com.example.auth_service.entity.Account;
import org.springframework.http.ResponseEntity;

public interface AccountService {
    Account createAccount(Account account);
    UserPrincipal findByUsername(String username);
    boolean isExist(String username);
    boolean isValidPassword(String password);
    ResponseEntity<Object> createUser(Long accountId);
}
