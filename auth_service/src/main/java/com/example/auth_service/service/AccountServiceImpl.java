package com.example.auth_service.service;

import com.example.auth_service.authen.UserPrincipal;
import com.example.auth_service.entity.Account;
import com.example.auth_service.entity.Role;
import com.example.auth_service.repositories.AccountRepository;
import com.example.auth_service.repositories.RoleRepository;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.RequestNotPermitted;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class AccountServiceImpl implements AccountService {
    @Value("${user.service.url}")
    private String userServiceApi;

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private RoleRepository roleRepository;
    RestTemplate restTemplate = new RestTemplate();

    @Override
    public Account createAccount(Account account) {
        return accountRepository.saveAndFlush(account);
    }

    @Override
    public UserPrincipal findByUsername(String username) {
        Account account = accountRepository.findByUsername(username);
        UserPrincipal userPrincipal = new UserPrincipal();

        if (null != account) {

            Set<String> authorities = new HashSet<>();

            if (null != account.getRoles())

                account.getRoles().forEach(r -> {
                    authorities.add(r.getRoleKey());
                    r.getPermissions().forEach(
                            p -> authorities.add(p.getPermissionKey()));
                });

            userPrincipal.setUserId(account.getId());
            userPrincipal.setUsername(account.getUsername());
            userPrincipal.setPassword(account.getPassword());
            userPrincipal.setAuthorities(authorities);

        }

        return userPrincipal;

    }

    @Override
    public boolean isExist(String username) {
        Account account = accountRepository.findByUsername(username);
        return account != null;
    }

    @Override
    public boolean isValidPassword(String password) {
        String regex = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=.*[@#$%^&+=])"
                + "(?=\\S+$).{8,20}$";

        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(password);

        return m.matches();
    }

    @Override
    @Retry(name = "retryApi")
//    @RateLimiter(name = "rateLimiterApi", fallbackMethod = "rateLimiterFallback")
    @CircuitBreaker(name = "circuitBreakerApi", fallbackMethod = "circuitBreakerFallback")
    public ResponseEntity<Object> createUser(Long accountId) {
        Map<String, Object> data = new HashMap<>();
        data.put("accountId", accountId);
        HttpEntity<Object> request = new HttpEntity<>(data);
        ResponseEntity<Object> responseEntity = restTemplate.exchange(
                userServiceApi + "/create",
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<Object>() {}
        );
        return  responseEntity;
    }

    public ResponseEntity<String> rateLimiterFallback(Long accountId, RequestNotPermitted e) {
        // Return a suitable response when the circuit breaker is open
        return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).body("Request is not permitted");
    }

    public ResponseEntity<String> circuitBreakerFallback(Long accountId, CallNotPermittedException e) {
        // Return a suitable response when the circuit breaker is open
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("CircuitBreaker 'circuitBreakerApi' is OPEN and does not permit further calls");
    }
}
