package com.example.auth_service.service;

import com.example.auth_service.entity.Token;

public interface TokenService {
    Token createToken(Token token);

    Token findByToken(String token);
}