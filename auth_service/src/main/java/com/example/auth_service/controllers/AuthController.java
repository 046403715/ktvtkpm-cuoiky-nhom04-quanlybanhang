package com.example.auth_service.controllers;

import com.example.auth_service.authen.UserPrincipal;
import com.example.auth_service.entity.Token;
import com.example.auth_service.entity.Account;
import com.example.auth_service.service.TokenService;
import com.example.auth_service.service.AccountService;
import com.example.auth_service.util.JwtUtil;
import io.github.resilience4j.ratelimiter.RequestNotPermitted;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import jakarta.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private TokenService tokenService;

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody Account account) {
        try {
            if (account.getUsername() == null || account.getPassword() == null || account.getUsername().trim().isEmpty() || account.getPassword().trim().isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username and password are required and not empty");
            }
            if (accountService.isExist(account.getUsername())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username is already exist");
            }
            if (!accountService.isValidPassword(account.getPassword())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Please enter a valid password. The password must contain at least one digit, one lowercase letter, one uppercase letter, one special character, no whitespace, and must be 8 to 20 characters in length.");
            }
            account.setPassword(new BCryptPasswordEncoder().encode(account.getPassword()));
            Account savedAccount = accountService.createAccount(account);
            if (savedAccount == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Can't create this account");
            }
            return accountService.createUser(savedAccount.getId());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something is error from server");
        }
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody Account account, HttpSession session){

        UserPrincipal userPrincipal =
                accountService.findByUsername(account.getUsername());

        if (null == account || !new BCryptPasswordEncoder()
                .matches(account.getPassword(), userPrincipal.getPassword())) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Account or password is not valid!");
        }

        Token token = new Token();
        token.setToken(jwtUtil.generateToken(userPrincipal));

        token.setTokenExpDate(jwtUtil.generateExpirationDate());
        token.setCreatedBy(userPrincipal.getUserId());
        tokenService.createToken(token);

        session.setAttribute("token", token.getToken());

        return ResponseEntity.ok(token.getToken());
    }
}
